# Mr. Green's Weird Website

While investigating Mr. Green for something completely unrelated, we found this login page. Maybe you can find a way in?

This challenge was simply a login page with default creds. Entering admin for the username and admin for the password granted the flag.